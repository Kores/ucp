//! CLI helper for comparison predicate representation and syntax parsing.
//!
//! ## Use
//!
//! It can be used to parse CLI options such as:
//!
//! ```sh
//! list-files --size=gt:1024 # list files with size greater than 1024 bytes (1 KiB)
//! list-files --size=eq:1024 # list files with size equals to 1024 bytes (1 KiB)
//! list-files --size >1024 # list files with size greater than 1024 bytes (1 KiB)
//! ```
//!
//! ## Syntax options
//!
//! There are three syntax options: [`Textual`], [`Symbol`] and [`Combined`].
//!
//! ### [`Textual`]
//!
//! Textual comparison: `ne`, `eq`, `gt`, `lt`, `gte`, `lte`.
//!
//! #### Example
//!
//! ```sh
//! list-files --size=gte:1024 # list files with size greater than or equal to 1024 bytes (1 KiB)
//! ```
//!
//!
//! ### [`Symbol`]
//!
//! Comparison with operator characters: `!` (or `≠` for not equals),
//! `=` (or empty for equal), `>`, `<`, `>=`, `<=`.
//!
//! #### Example
//!
//! ```sh
//! list-files --size'=1024' # list files with size equal to 1024 bytes (1 KiB)
//! list-files --size=1024 # list files with size equal to 1024 bytes (1 KiB)
//! list-files --size'>1024' # list files with size greater than 1024 bytes (1 KiB)
//! ```
//!
//! ### [`Combined`]
//!
//! Combines both [`Textual`] and [`Symbol`] syntax. It tries to parse first as [`Textual`],
//! then as [`Symbol`].
//!

pub(super) mod parser;

use crate::pred::cmp::parser::OpParser;
use std::fmt::Display;
use std::marker::PhantomData;
use std::str::FromStr;

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Textual(());
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Symbol(());
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Combined(());

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Comparison<V, S = Combined> {
    operator: Operator,
    value: V,
    _marker: PhantomData<S>,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum Operator {
    Ne,
    Eq,
    Gt,
    Lt,
    Gte,
    Lte,
}

impl<V, S> Comparison<V, S>
where
    V: PartialOrd,
{
    pub fn test(&self, value: V) -> bool {
        match self.operator {
            Operator::Ne => value != self.value,
            Operator::Eq => value == self.value,
            Operator::Gt => value > self.value,
            Operator::Lt => value < self.value,
            Operator::Gte => value >= self.value,
            Operator::Lte => value <= self.value,
        }
    }
}

impl<V, S> Comparison<V, S> {
    pub fn new(operator: Operator, value: V) -> Self {
        Self {
            operator,
            value,
            _marker: PhantomData,
        }
    }

    pub fn operator(&self) -> Operator {
        self.operator
    }

    pub fn value(&self) -> &V {
        &self.value
    }

    pub fn into_inner(self) -> (Operator, V) {
        (self.operator, self.value)
    }
}

impl<V> FromStr for Comparison<V, Textual>
where
    V: FromStr,
    <V as FromStr>::Err: Display,
{
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts = s.splitn(2, ':').collect::<Vec<&str>>();
        if parts.len() != 2 {
            Err("Invalid format".to_string())
        } else {
            let operator = match parts[0].trim() {
                "ne" => Operator::Ne,
                "eq" => Operator::Eq,
                "gt" => Operator::Gt,
                "lt" => Operator::Lt,
                "gte" => Operator::Gte,
                "lte" => Operator::Lte,
                _ => return Err(format!("invalid operator: {}", parts[0])),
            };
            let value = parts[1]
                .trim_start_matches(' ')
                .parse::<V>()
                .map_err(|e| e.to_string())?;
            Ok(Comparison::new(operator, value))
        }
    }
}

impl<V> FromStr for Comparison<V, Symbol>
where
    V: FromStr,
    <V as FromStr>::Err: Display,
{
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (operator, new_str) = OpParser::simple_op_parser(s).map_err(|e| e.to_string())?;
        let value = new_str
            .trim_start_matches(' ')
            .parse::<V>()
            .map_err(|e| e.to_string())?;
        Ok(Comparison::new(operator, value))
    }
}

impl<V> FromStr for Comparison<V, Combined>
where
    V: FromStr,
    <V as FromStr>::Err: Display,
{
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse::<Comparison<V, Textual>>()
            .map(|v| Comparison::<V, Combined>::new(v.operator, v.value))
            .or_else(|textual| s.parse::<Comparison<V, Symbol>>()
                .map(|v| Comparison::<V, Combined>::new(v.operator, v.value))
                .map_err(|op_err| {
                format!("failed to parse textual: {textual}, failed to parse with operator: {op_err}")
            }))
    }
}

#[cfg(test)]
mod tests {
    macro_rules! test_cases {
        (syntax = $syntax:ty; value_type = $value_type:ty; $($operator:expr => [$($input:literal),+] = $expected:literal,)+) => {$($(
            {
                let input = $input;
                let pred = input.parse::<Comparison<$value_type, $syntax>>().unwrap();
                assert_eq!(pred.operator(), $operator, "input: {input}");
                assert_eq!(pred.value(), &$expected, "input: {input}");
            }
        )+)+};
    }

    use crate::pred::{cmp::Operator, Combined, Comparison, Symbol, Textual};

    #[test]
    fn test_symbol() {
        test_cases!(
            syntax = Symbol;
            value_type = usize;
            Operator::Eq => ["1024", " =1024", " = 1024"] = 1024,
            Operator::Ne => ["≠1024", "≠ 1024", "!1024", " ! 1024"] = 1024,
            Operator::Gt => [">1024", " >1024", "  >1024"] = 1024,
            Operator::Gte => [">=1024", " >=1024", " >= 1024"] = 1024,
            Operator::Lt => ["<1024", " <1024", "  <1024"] = 1024,
            Operator::Lte => ["<=1024", " <=1024", "  <=1024"] = 1024,
        );
    }

    #[test]
    fn test_textual() {
        test_cases!(
            syntax = Textual;
            value_type = usize;
            Operator::Eq => ["eq:1024", "eq: 1024", " eq: 1024"] = 1024,
            Operator::Ne => ["ne:1024", "ne: 1024", " ne: 1024"] = 1024,
            Operator::Gt => ["gt:1024", "gt: 1024", " gt: 1024"] = 1024,
            Operator::Gte => ["gte:1024", "gte: 1024", " gte: 1024"] = 1024,
            Operator::Lt => ["lt:1024", "lt: 1024", " lt: 1024"] = 1024,
            Operator::Lte => ["lte:1024", "lte: 1024", " lte: 1024"] = 1024,
        );
    }

    #[test]
    fn test_combined() {
        test_cases!(
            syntax = Combined;
            value_type = usize;
            Operator::Eq => ["eq:1024", "=1024", "1024"] = 1024,
            Operator::Ne => ["ne:1024", "!1024", "≠1024"] = 1024,
            Operator::Gt => ["gt:1024", ">1024"] = 1024,
            Operator::Gte => ["gte:1024", ">=1024"] = 1024,
            Operator::Lt => ["lt:1024", "<1024"] = 1024,
            Operator::Lte => ["lte:1024", "<=1024"] = 1024,
        );
    }
}
