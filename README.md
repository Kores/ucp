# Useful CLI Parsers (UCP)

This crate provides a set of parsers for command line interfaces (CLI).

## Examples

*Nothing explains better than examples.*

```rust
use ucp::pred::{Comparison, FieldComparison, Operator};

fn main() {
    assert_eq!(
        ">=1024".parse::<Comparison<usize>>(),
        Ok(Comparison::new(Operator::Gte, 1024))
    );

    assert_eq!(
        "!1024".parse::<Comparison<usize>>(),
        Ok(Comparison::new(Operator::Ne, 1024))
    );

    assert_eq!(
        "size=1024".parse::<FieldComparison<String, usize>>(),
        Ok(FieldComparison::new(
            String::from("size"),
            Operator::Eq,
            1024
        )),
    );

    assert_eq!(
        "size≠1024".parse::<FieldComparison<String, usize>>(),
        Ok(FieldComparison::new(
            String::from("size"),
            Operator::Ne,
            1024
        )),
    );
}

```

## Note

I made this crate to simplify the process of making cli tools (which I do a lot for myself), it's intended to be used
with [clap](https://crates.io/crates/clap) and has no specialized `Error` type for parsing failures, `String` was
used instead.

This may change in the next versions of the crate and will break compatibility with previous versions.